--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actividad; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.actividad (
    id_actividad integer NOT NULL,
    nombre character varying(50) NOT NULL,
    cupos smallint,
    lugar character varying(20) NOT NULL,
    rut_profesor character varying(9) NOT NULL,
    CONSTRAINT actividad_cupos_check CHECK ((cupos > 0)),
    CONSTRAINT actividad_rut_profesor_check CHECK (((rut_profesor)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.actividad OWNER TO cromer;

--
-- Name: actividad_bloque; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.actividad_bloque (
    id_actividad integer NOT NULL,
    id_bloque integer NOT NULL
);


ALTER TABLE public.actividad_bloque OWNER TO cromer;

--
-- Name: actividad_id_actividad_seq; Type: SEQUENCE; Schema: public; Owner: cromer
--

CREATE SEQUENCE public.actividad_id_actividad_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.actividad_id_actividad_seq OWNER TO cromer;

--
-- Name: actividad_id_actividad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cromer
--

ALTER SEQUENCE public.actividad_id_actividad_seq OWNED BY public.actividad.id_actividad;


--
-- Name: actividad_nivel; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.actividad_nivel (
    id_actividad integer NOT NULL,
    id_nivel integer NOT NULL
);


ALTER TABLE public.actividad_nivel OWNER TO cromer;

--
-- Name: alumno; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.alumno (
    rut_alumno character varying(9) NOT NULL,
    nombres character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    direccion character varying(30) NOT NULL,
    id_ciudad smallint,
    anyo_ingreso smallint,
    fecha_nacimiento date NOT NULL,
    rut_apoderado character varying(9) NOT NULL,
    CONSTRAINT alumno_anyo_ingreso_check CHECK ((anyo_ingreso > 1980)),
    CONSTRAINT alumno_fecha_nacimiento_check CHECK ((fecha_nacimiento > '1980-01-01'::date)),
    CONSTRAINT alumno_rut_alumno_check CHECK (((rut_alumno)::text ~ '^[0-9k]+$'::text)),
    CONSTRAINT alumno_rut_apoderado_check CHECK (((rut_apoderado)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.alumno OWNER TO cromer;

--
-- Name: apoderado; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.apoderado (
    rut_apoderado character varying(9) NOT NULL,
    nombres character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    direccion character varying(30) NOT NULL,
    id_ciudad smallint,
    CONSTRAINT apoderado_rut_apoderado_check CHECK (((rut_apoderado)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.apoderado OWNER TO cromer;

--
-- Name: asignatura; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.asignatura (
    id_asignatura integer NOT NULL,
    nombre character varying(50) NOT NULL,
    rut_profesor character varying(9) NOT NULL,
    CONSTRAINT asignatura_rut_profesor_check CHECK (((rut_profesor)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.asignatura OWNER TO cromer;

--
-- Name: asignatura_id_asignatura_seq; Type: SEQUENCE; Schema: public; Owner: cromer
--

CREATE SEQUENCE public.asignatura_id_asignatura_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asignatura_id_asignatura_seq OWNER TO cromer;

--
-- Name: asignatura_id_asignatura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cromer
--

ALTER SEQUENCE public.asignatura_id_asignatura_seq OWNED BY public.asignatura.id_asignatura;


--
-- Name: asistente; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.asistente (
    rut_profesor character varying(9) NOT NULL,
    id_curso character varying(10) NOT NULL,
    CONSTRAINT asistente_rut_profesor_check CHECK (((rut_profesor)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.asistente OWNER TO cromer;

--
-- Name: asociado; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.asociado (
    id_curso character varying(10) NOT NULL,
    id_asignatura integer NOT NULL
);


ALTER TABLE public.asociado OWNER TO cromer;

--
-- Name: bloque; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.bloque (
    id_bloque integer NOT NULL,
    descripcion character varying(20) NOT NULL
);


ALTER TABLE public.bloque OWNER TO cromer;

--
-- Name: bloque_id_bloque_seq; Type: SEQUENCE; Schema: public; Owner: cromer
--

CREATE SEQUENCE public.bloque_id_bloque_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bloque_id_bloque_seq OWNER TO cromer;

--
-- Name: bloque_id_bloque_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cromer
--

ALTER SEQUENCE public.bloque_id_bloque_seq OWNED BY public.bloque.id_bloque;


--
-- Name: ciudad; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.ciudad (
    id_ciudad smallint NOT NULL,
    nombre_ciudad character varying(20) NOT NULL,
    id_region smallint
);


ALTER TABLE public.ciudad OWNER TO cromer;

--
-- Name: ciudad_id_ciudad_seq; Type: SEQUENCE; Schema: public; Owner: cromer
--

CREATE SEQUENCE public.ciudad_id_ciudad_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ciudad_id_ciudad_seq OWNER TO cromer;

--
-- Name: ciudad_id_ciudad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cromer
--

ALTER SEQUENCE public.ciudad_id_ciudad_seq OWNED BY public.ciudad.id_ciudad;


--
-- Name: cursar; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.cursar (
    rut_alumno character varying(9) NOT NULL,
    id_curso character varying(10) NOT NULL,
    CONSTRAINT cursar_rut_alumno_check CHECK (((rut_alumno)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.cursar OWNER TO cromer;

--
-- Name: curso; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.curso (
    id_curso character varying(10) NOT NULL,
    nombre character varying(6) NOT NULL,
    anyo smallint,
    rut_profesor character varying(9) NOT NULL,
    CONSTRAINT curso_anyo_check CHECK ((anyo > 1980)),
    CONSTRAINT curso_rut_profesor_check CHECK (((rut_profesor)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.curso OWNER TO cromer;

--
-- Name: horario; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.horario (
    id_asignatura integer NOT NULL,
    id_bloque integer NOT NULL,
    sala character varying(20) NOT NULL
);


ALTER TABLE public.horario OWNER TO cromer;

--
-- Name: nivel; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.nivel (
    id_nivel integer NOT NULL,
    nombre character varying(20) NOT NULL
);


ALTER TABLE public.nivel OWNER TO cromer;

--
-- Name: nivel_id_nivel_seq; Type: SEQUENCE; Schema: public; Owner: cromer
--

CREATE SEQUENCE public.nivel_id_nivel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_id_nivel_seq OWNER TO cromer;

--
-- Name: nivel_id_nivel_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cromer
--

ALTER SEQUENCE public.nivel_id_nivel_seq OWNED BY public.nivel.id_nivel;


--
-- Name: participar; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.participar (
    rut_alumno character varying(9) NOT NULL,
    id_actividad integer NOT NULL,
    CONSTRAINT participar_rut_alumno_check CHECK (((rut_alumno)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.participar OWNER TO cromer;

--
-- Name: profesor; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.profesor (
    rut_profesor character varying(9) NOT NULL,
    nombres character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    direccion character varying(30) NOT NULL,
    id_ciudad smallint,
    CONSTRAINT profesor_rut_profesor_check CHECK (((rut_profesor)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.profesor OWNER TO cromer;

--
-- Name: region; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.region (
    id_region smallint NOT NULL,
    nombre_region character varying(20) NOT NULL
);


ALTER TABLE public.region OWNER TO cromer;

--
-- Name: region_id_region_seq; Type: SEQUENCE; Schema: public; Owner: cromer
--

CREATE SEQUENCE public.region_id_region_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.region_id_region_seq OWNER TO cromer;

--
-- Name: region_id_region_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cromer
--

ALTER SEQUENCE public.region_id_region_seq OWNED BY public.region.id_region;


--
-- Name: registro; Type: TABLE; Schema: public; Owner: cromer
--

CREATE TABLE public.registro (
    rut_alumno character varying(9) NOT NULL,
    id_asignatura integer NOT NULL,
    nota numeric(2,1),
    CONSTRAINT registro_nota_check CHECK (((nota >= 1.0) AND (nota <= 7.0))),
    CONSTRAINT registro_rut_alumno_check CHECK (((rut_alumno)::text ~ '^[0-9k]+$'::text))
);


ALTER TABLE public.registro OWNER TO cromer;

--
-- Name: actividad id_actividad; Type: DEFAULT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad ALTER COLUMN id_actividad SET DEFAULT nextval('public.actividad_id_actividad_seq'::regclass);


--
-- Name: asignatura id_asignatura; Type: DEFAULT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asignatura ALTER COLUMN id_asignatura SET DEFAULT nextval('public.asignatura_id_asignatura_seq'::regclass);


--
-- Name: bloque id_bloque; Type: DEFAULT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.bloque ALTER COLUMN id_bloque SET DEFAULT nextval('public.bloque_id_bloque_seq'::regclass);


--
-- Name: ciudad id_ciudad; Type: DEFAULT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.ciudad ALTER COLUMN id_ciudad SET DEFAULT nextval('public.ciudad_id_ciudad_seq'::regclass);


--
-- Name: nivel id_nivel; Type: DEFAULT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.nivel ALTER COLUMN id_nivel SET DEFAULT nextval('public.nivel_id_nivel_seq'::regclass);


--
-- Name: region id_region; Type: DEFAULT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.region ALTER COLUMN id_region SET DEFAULT nextval('public.region_id_region_seq'::regclass);


--
-- Data for Name: actividad; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.actividad (id_actividad, nombre, cupos, lugar, rut_profesor) FROM stdin;
1	Quidditch	14	Quidditch field	226174788
2	Ogre taming	5	The forbidden forest	226779329
3	Advanced transfiguration	2	Headmaster's office	139414853
4	Rugby	30	Rugby field	226174788
\.


--
-- Data for Name: actividad_bloque; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.actividad_bloque (id_actividad, id_bloque) FROM stdin;
1	1
2	2
3	3
\.


--
-- Data for Name: actividad_nivel; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.actividad_nivel (id_actividad, id_nivel) FROM stdin;
1	1
2	2
3	3
\.


--
-- Data for Name: alumno; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.alumno (rut_alumno, nombres, apellidos, direccion, id_ciudad, anyo_ingreso, fecha_nacimiento, rut_apoderado) FROM stdin;
254049492	Isabella Lynn	Cromer Chacano	214 Edison Dr.	1	2019	2019-10-04	14592927k
245191391	Tomas	Bunt	Avenida San Sebastian 778	2	2018	2010-02-05	192186099
18028642k	Tom	Riddle	320 Thomas Jefferson St.	3	2017	2014-12-31	192199859
22690395k	Draco	Malfoy	743 Thames Ave.	1	2017	2006-10-04	192199859
215221350	Victor	Krum	9282 Park Way	1	2017	2007-02-12	192199859
144715179	Pansy	Parkinson	339 Mobile Hwy.	1	2017	2008-12-03	192199859
86210681	Oliver	Wood	323 Crane Ave.	1	2017	2007-01-02	192199859
20084453k	Theodore	Nott	848 Short Way	1	2017	2006-02-04	192199859
133765859	Cormac	McLaggen	484 9th Ave.	1	2017	2007-01-08	192199859
\.


--
-- Data for Name: apoderado; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.apoderado (rut_apoderado, nombres, apellidos, direccion, id_ciudad) FROM stdin;
14592927k	Christopher Barry	Cromer	214 Edison Dr.	1
192186099	Elia Alejandra	Chacano Troncoso	Avenida San Sebastian 778	2
192199859	Jack	Johnson	320 Thomas Jefferson St.	3
\.


--
-- Data for Name: asignatura; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.asignatura (id_asignatura, nombre, rut_profesor) FROM stdin;
1	Transfiguration	226779329
2	Defense against the dark arts	226174788
3	Muggle studies	139414853
4	Potions	226174788
\.


--
-- Data for Name: asistente; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.asistente (rut_profesor, id_curso) FROM stdin;
139414853	1A-2019
226174788	2B-2019
\.


--
-- Data for Name: asociado; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.asociado (id_curso, id_asignatura) FROM stdin;
1A-2019	1
2B-2019	2
3C-2019	3
6A-2018	1
6A-2018	2
6A-2018	3
1A-2018	4
\.


--
-- Data for Name: bloque; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.bloque (id_bloque, descripcion) FROM stdin;
1	08:10 - 09:30
2	09:40 - 11:00
3	11:10 - 12:30
4	12:40 - 14:00
5	14:10 - 15:30
6	15:40 - 17:00
7	17:10 - 18:30
8	18:40 - 20:00
\.


--
-- Data for Name: ciudad; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.ciudad (id_ciudad, nombre_ciudad, id_region) FROM stdin;
1	Pensacola	1
2	Montgomery	2
3	Virginia Beach	3
\.


--
-- Data for Name: cursar; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.cursar (rut_alumno, id_curso) FROM stdin;
254049492	1A-2019
245191391	2B-2019
18028642k	3C-2019
22690395k	6A-2018
215221350	6A-2018
144715179	6A-2018
86210681	2B-2019
20084453k	2B-2019
133765859	3C-2019
\.


--
-- Data for Name: curso; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.curso (id_curso, nombre, anyo, rut_profesor) FROM stdin;
1A-2019	1A	2019	226174788
2B-2019	2B	2019	226779329
3C-2019	3C	2019	139414853
6A-2018	6A	2018	226174788
1A-2018	1A	2018	226779329
\.


--
-- Data for Name: horario; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.horario (id_asignatura, id_bloque, sala) FROM stdin;
2	2	301AB
3	3	102AA
1	1	203AC
4	8	105AA
\.


--
-- Data for Name: nivel; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.nivel (id_nivel, nombre) FROM stdin;
1	Primero
2	Segundo
3	Tercero
\.


--
-- Data for Name: participar; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.participar (rut_alumno, id_actividad) FROM stdin;
254049492	1
245191391	2
18028642k	3
22690395k	1
215221350	1
144715179	1
86210681	1
20084453k	1
133765859	1
22690395k	4
\.


--
-- Data for Name: profesor; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.profesor (rut_profesor, nombres, apellidos, direccion, id_ciudad) FROM stdin;
226174788	Severus	Snape	232 Jackson Ave.	1
226779329	Minerva	McGonagall	333 Nottingham Ln.	2
139414853	Albus	Dumbledore	8450 Sparrow Rd.	3
\.


--
-- Data for Name: region; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.region (id_region, nombre_region) FROM stdin;
1	Bío Bío
2	Ñuble
3	Virginia
\.


--
-- Data for Name: registro; Type: TABLE DATA; Schema: public; Owner: cromer
--

COPY public.registro (rut_alumno, id_asignatura, nota) FROM stdin;
254049492	1	7.0
18028642k	3	1.0
22690395k	4	3.0
144715179	4	2.0
245191391	2	4.1
86210681	4	5.0
\.


--
-- Name: actividad_id_actividad_seq; Type: SEQUENCE SET; Schema: public; Owner: cromer
--

SELECT pg_catalog.setval('public.actividad_id_actividad_seq', 1, false);


--
-- Name: asignatura_id_asignatura_seq; Type: SEQUENCE SET; Schema: public; Owner: cromer
--

SELECT pg_catalog.setval('public.asignatura_id_asignatura_seq', 13, true);


--
-- Name: bloque_id_bloque_seq; Type: SEQUENCE SET; Schema: public; Owner: cromer
--

SELECT pg_catalog.setval('public.bloque_id_bloque_seq', 1, false);


--
-- Name: ciudad_id_ciudad_seq; Type: SEQUENCE SET; Schema: public; Owner: cromer
--

SELECT pg_catalog.setval('public.ciudad_id_ciudad_seq', 1, false);


--
-- Name: nivel_id_nivel_seq; Type: SEQUENCE SET; Schema: public; Owner: cromer
--

SELECT pg_catalog.setval('public.nivel_id_nivel_seq', 1, false);


--
-- Name: region_id_region_seq; Type: SEQUENCE SET; Schema: public; Owner: cromer
--

SELECT pg_catalog.setval('public.region_id_region_seq', 1, false);


--
-- Name: actividad_bloque actividad_bloque_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad_bloque
    ADD CONSTRAINT actividad_bloque_pkey PRIMARY KEY (id_actividad, id_bloque);


--
-- Name: actividad_nivel actividad_nivel_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad_nivel
    ADD CONSTRAINT actividad_nivel_pkey PRIMARY KEY (id_actividad, id_nivel);


--
-- Name: actividad actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad
    ADD CONSTRAINT actividad_pkey PRIMARY KEY (id_actividad);


--
-- Name: alumno alumno_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT alumno_pkey PRIMARY KEY (rut_alumno);


--
-- Name: apoderado apoderado_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.apoderado
    ADD CONSTRAINT apoderado_pkey PRIMARY KEY (rut_apoderado);


--
-- Name: asignatura asignatura_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asignatura
    ADD CONSTRAINT asignatura_pkey PRIMARY KEY (id_asignatura);


--
-- Name: asistente asistente_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asistente
    ADD CONSTRAINT asistente_pkey PRIMARY KEY (rut_profesor, id_curso);


--
-- Name: asociado asociado_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asociado
    ADD CONSTRAINT asociado_pkey PRIMARY KEY (id_curso, id_asignatura);


--
-- Name: bloque bloque_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.bloque
    ADD CONSTRAINT bloque_pkey PRIMARY KEY (id_bloque);


--
-- Name: ciudad ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_pkey PRIMARY KEY (id_ciudad);


--
-- Name: cursar cursar_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.cursar
    ADD CONSTRAINT cursar_pkey PRIMARY KEY (rut_alumno, id_curso);


--
-- Name: curso curso_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.curso
    ADD CONSTRAINT curso_pkey PRIMARY KEY (id_curso);


--
-- Name: horario horario_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.horario
    ADD CONSTRAINT horario_pkey PRIMARY KEY (id_asignatura, id_bloque);


--
-- Name: nivel nivel_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.nivel
    ADD CONSTRAINT nivel_pkey PRIMARY KEY (id_nivel);


--
-- Name: participar participar_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.participar
    ADD CONSTRAINT participar_pkey PRIMARY KEY (rut_alumno, id_actividad);


--
-- Name: profesor profesor_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.profesor
    ADD CONSTRAINT profesor_pkey PRIMARY KEY (rut_profesor);


--
-- Name: region region_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id_region);


--
-- Name: registro registro_pkey; Type: CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.registro
    ADD CONSTRAINT registro_pkey PRIMARY KEY (rut_alumno, id_asignatura);


--
-- Name: actividad_bloque actividad_bloque_id_actividad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad_bloque
    ADD CONSTRAINT actividad_bloque_id_actividad_fkey FOREIGN KEY (id_actividad) REFERENCES public.actividad(id_actividad) ON DELETE RESTRICT;


--
-- Name: actividad_bloque actividad_bloque_id_bloque_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad_bloque
    ADD CONSTRAINT actividad_bloque_id_bloque_fkey FOREIGN KEY (id_bloque) REFERENCES public.bloque(id_bloque) ON DELETE RESTRICT;


--
-- Name: actividad_nivel actividad_nivel_id_actividad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad_nivel
    ADD CONSTRAINT actividad_nivel_id_actividad_fkey FOREIGN KEY (id_actividad) REFERENCES public.actividad(id_actividad) ON DELETE RESTRICT;


--
-- Name: actividad_nivel actividad_nivel_id_nivel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad_nivel
    ADD CONSTRAINT actividad_nivel_id_nivel_fkey FOREIGN KEY (id_nivel) REFERENCES public.nivel(id_nivel) ON DELETE RESTRICT;


--
-- Name: actividad actividad_rut_profesor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.actividad
    ADD CONSTRAINT actividad_rut_profesor_fkey FOREIGN KEY (rut_profesor) REFERENCES public.profesor(rut_profesor) ON DELETE RESTRICT;


--
-- Name: alumno alumno_id_ciudad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT alumno_id_ciudad_fkey FOREIGN KEY (id_ciudad) REFERENCES public.ciudad(id_ciudad) ON DELETE RESTRICT;


--
-- Name: alumno alumno_rut_apoderado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT alumno_rut_apoderado_fkey FOREIGN KEY (rut_apoderado) REFERENCES public.apoderado(rut_apoderado) ON DELETE RESTRICT;


--
-- Name: apoderado apoderado_id_ciudad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.apoderado
    ADD CONSTRAINT apoderado_id_ciudad_fkey FOREIGN KEY (id_ciudad) REFERENCES public.ciudad(id_ciudad) ON DELETE RESTRICT;


--
-- Name: asignatura asignatura_rut_profesor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asignatura
    ADD CONSTRAINT asignatura_rut_profesor_fkey FOREIGN KEY (rut_profesor) REFERENCES public.profesor(rut_profesor) ON DELETE RESTRICT;


--
-- Name: asistente asistente_id_curso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asistente
    ADD CONSTRAINT asistente_id_curso_fkey FOREIGN KEY (id_curso) REFERENCES public.curso(id_curso) ON DELETE RESTRICT;


--
-- Name: asistente asistente_rut_profesor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asistente
    ADD CONSTRAINT asistente_rut_profesor_fkey FOREIGN KEY (rut_profesor) REFERENCES public.profesor(rut_profesor) ON DELETE RESTRICT;


--
-- Name: asociado asociado_id_asignatura_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asociado
    ADD CONSTRAINT asociado_id_asignatura_fkey FOREIGN KEY (id_asignatura) REFERENCES public.asignatura(id_asignatura) ON DELETE RESTRICT;


--
-- Name: asociado asociado_id_curso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.asociado
    ADD CONSTRAINT asociado_id_curso_fkey FOREIGN KEY (id_curso) REFERENCES public.curso(id_curso) ON DELETE RESTRICT;


--
-- Name: ciudad ciudad_id_region_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.ciudad
    ADD CONSTRAINT ciudad_id_region_fkey FOREIGN KEY (id_region) REFERENCES public.region(id_region) ON DELETE RESTRICT;


--
-- Name: cursar cursar_id_curso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.cursar
    ADD CONSTRAINT cursar_id_curso_fkey FOREIGN KEY (id_curso) REFERENCES public.curso(id_curso) ON DELETE RESTRICT;


--
-- Name: cursar cursar_rut_alumno_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.cursar
    ADD CONSTRAINT cursar_rut_alumno_fkey FOREIGN KEY (rut_alumno) REFERENCES public.alumno(rut_alumno) ON DELETE RESTRICT;


--
-- Name: curso curso_rut_profesor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.curso
    ADD CONSTRAINT curso_rut_profesor_fkey FOREIGN KEY (rut_profesor) REFERENCES public.profesor(rut_profesor) ON DELETE RESTRICT;


--
-- Name: horario horario_id_asignatura_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.horario
    ADD CONSTRAINT horario_id_asignatura_fkey FOREIGN KEY (id_asignatura) REFERENCES public.asignatura(id_asignatura) ON DELETE RESTRICT;


--
-- Name: horario horario_id_bloque_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.horario
    ADD CONSTRAINT horario_id_bloque_fkey FOREIGN KEY (id_bloque) REFERENCES public.bloque(id_bloque) ON DELETE RESTRICT;


--
-- Name: participar participar_id_actividad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.participar
    ADD CONSTRAINT participar_id_actividad_fkey FOREIGN KEY (id_actividad) REFERENCES public.actividad(id_actividad) ON DELETE RESTRICT;


--
-- Name: participar participar_rut_alumno_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.participar
    ADD CONSTRAINT participar_rut_alumno_fkey FOREIGN KEY (rut_alumno) REFERENCES public.alumno(rut_alumno) ON DELETE RESTRICT;


--
-- Name: profesor profesor_id_ciudad_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.profesor
    ADD CONSTRAINT profesor_id_ciudad_fkey FOREIGN KEY (id_ciudad) REFERENCES public.ciudad(id_ciudad) ON DELETE RESTRICT;


--
-- Name: registro registro_id_asignatura_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.registro
    ADD CONSTRAINT registro_id_asignatura_fkey FOREIGN KEY (id_asignatura) REFERENCES public.asignatura(id_asignatura) ON DELETE RESTRICT;


--
-- Name: registro registro_rut_alumno_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cromer
--

ALTER TABLE ONLY public.registro
    ADD CONSTRAINT registro_rut_alumno_fkey FOREIGN KEY (rut_alumno) REFERENCES public.alumno(rut_alumno) ON DELETE RESTRICT;


--
-- Name: TABLE actividad; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.actividad TO bdd;


--
-- Name: TABLE actividad_bloque; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.actividad_bloque TO bdd;


--
-- Name: SEQUENCE actividad_id_actividad_seq; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON SEQUENCE public.actividad_id_actividad_seq TO bdd;


--
-- Name: TABLE actividad_nivel; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.actividad_nivel TO bdd;


--
-- Name: TABLE alumno; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.alumno TO bdd;


--
-- Name: TABLE apoderado; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.apoderado TO bdd;


--
-- Name: TABLE asignatura; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.asignatura TO bdd;


--
-- Name: SEQUENCE asignatura_id_asignatura_seq; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON SEQUENCE public.asignatura_id_asignatura_seq TO bdd;


--
-- Name: TABLE asistente; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.asistente TO bdd;


--
-- Name: TABLE asociado; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.asociado TO bdd;


--
-- Name: TABLE bloque; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.bloque TO bdd;


--
-- Name: SEQUENCE bloque_id_bloque_seq; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON SEQUENCE public.bloque_id_bloque_seq TO bdd;


--
-- Name: TABLE ciudad; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.ciudad TO bdd;


--
-- Name: SEQUENCE ciudad_id_ciudad_seq; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON SEQUENCE public.ciudad_id_ciudad_seq TO bdd;


--
-- Name: TABLE cursar; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.cursar TO bdd;


--
-- Name: TABLE curso; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.curso TO bdd;


--
-- Name: TABLE horario; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.horario TO bdd;


--
-- Name: TABLE nivel; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.nivel TO bdd;


--
-- Name: SEQUENCE nivel_id_nivel_seq; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON SEQUENCE public.nivel_id_nivel_seq TO bdd;


--
-- Name: TABLE participar; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.participar TO bdd;


--
-- Name: TABLE profesor; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.profesor TO bdd;


--
-- Name: TABLE region; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.region TO bdd;


--
-- Name: SEQUENCE region_id_region_seq; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON SEQUENCE public.region_id_region_seq TO bdd;


--
-- Name: TABLE registro; Type: ACL; Schema: public; Owner: cromer
--

GRANT ALL ON TABLE public.registro TO bdd;


--
-- PostgreSQL database dump complete
--

