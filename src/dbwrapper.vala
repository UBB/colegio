/*
 * Copyright 2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Colegio {
	namespace DB {
		namespace Wrapper {
			using Postgres;

			public errordomain Field {
				NOTFOUND
			}

			public class ResultWrapper : Object {
				public unowned Result result { get; set; default = null; }

				public ResultWrapper (Result? result = null) {
					this.result = result;
				}

				public string get_string_v (int tup_num, int field_num) {
					return result.get_value (tup_num, field_num);
				}

				public int get_int_v (int tup_num, int field_num) {
					return int.parse (result.get_value (tup_num, field_num));
				}

				public float get_float_v (int tup_num, int field_num) {
					return float.parse(result.get_value (tup_num, field_num));
				}

				public double get_double_v (int tup_num, int field_num) {
					return double.parse (result.get_value (tup_num, field_num));
				}

				public string get_string_n (int tup_num, string name) throws Field {
					return get_string_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				public int get_int_n (int tup_num, string name) throws Field {
					return get_int_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				public float get_float_n (int tup_num, string name) throws Field {
					return get_float_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				public double get_double_n (int tup_num, string name) throws Field {
					return get_double_v (tup_num, check_field_found (result.get_field_number (name), name));
				}

				private int check_field_found (int field_num, string name) throws Field {
					if (field_num == -1) {
						throw new Field.NOTFOUND ("El campo %s no estaba en los resultados de la consulta!", name);
					}
					return field_num;
				}
			}
		}
	}
}
