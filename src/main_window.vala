/*
 * Copyright 2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Colegio {
	using Constants;
	using DB;

	[GtkTemplate (ui = "/cl/cromer/ubb/colegio/main.window.ui")]
	public class MainWindow : Gtk.ApplicationWindow {
		private Connection conn;
		private Gtk.Grid content;

		[GtkChild]
		private Gtk.Box mainbox;

		[GtkChild]
		private Gtk.MenuItem asignaturas;

		[GtkChild]
		private Gtk.MenuItem horarios;

		[GtkChild]
		private Gtk.MenuItem curso;

		[GtkChild]
		private Gtk.MenuItem registro;

		[GtkChild]
		private Gtk.MenuItem q1;

		[GtkChild]
		private Gtk.MenuItem q2;

		[GtkChild]
		private Gtk.MenuItem q3;

		[GtkChild]
		private Gtk.MenuItem q4;

		[GtkChild]
		private Gtk.MenuItem q5;

		[GtkChild]
		private Gtk.MenuItem q6;

		[GtkChild]
		private Gtk.MenuItem quit;

		[GtkCallback]
		private void on_destroy(Gtk.Widget widget) {
			application.quit ();
		}

		[GtkCallback]
		private void on_activate_menu(Gtk.MenuItem menu_item) {
			if (menu_item == asignaturas) {
				var asignatura_window = new AsignaturaList (application, conn);
				asignatura_window.set_transient_for (this); // Set this window as the parent of the new window
				asignatura_window.initialize ();
				asignatura_window.show_all ();
			}
			else if (menu_item == horarios) {
				var horarios_window = new HorarioList (application, conn);
				horarios_window.set_transient_for (this); // Set this window as the parent of the new window
				horarios_window.initialize ();
				horarios_window.show_all ();
			}
			else if (menu_item == curso) {
				var asociado_window = new AsociadoList (application, conn);
				asociado_window.set_transient_for (this); // Set this window as the parent of the new window
				asociado_window.initialize ();
				asociado_window.show_all ();
			}
			else if (menu_item == registro) {
				var registro_window = new RegistroList (application, conn);
				registro_window.set_transient_for (this); // Set this window as the parent of the new window
				registro_window.initialize ();
				registro_window.show_all ();
			}
			else if (menu_item == q1) {
				var query_window = new QueryWindow (application, conn, QueryWindow.Query.Q1);
				query_window.set_transient_for (this); // Set this window as the parent of the new window
				query_window.initialize ();
				query_window.show_all ();
			}
			else if (menu_item == q2) {
				var query_window = new QueryWindow (application, conn, QueryWindow.Query.Q2);
				query_window.set_transient_for (this); // Set this window as the parent of the new window
				query_window.initialize ();
				query_window.show_all ();
			}
			else if (menu_item == q3) {
				var query_window = new QueryWindow (application, conn, QueryWindow.Query.Q3);
				query_window.set_transient_for (this); // Set this window as the parent of the new window
				query_window.initialize ();
				query_window.show_all ();
			}
			else if (menu_item == q4) {
				var query_window = new QueryWindow (application, conn, QueryWindow.Query.Q4);
				query_window.set_transient_for (this); // Set this window as the parent of the new window
				query_window.initialize ();
				query_window.show_all ();
			}
			else if (menu_item == q5) {
				var query_window = new QueryWindow (application, conn, QueryWindow.Query.Q5);
				query_window.set_transient_for (this); // Set this window as the parent of the new window
				query_window.initialize ();
				query_window.show_all ();
			}
			else if (menu_item == q6) {
				var query_window = new QueryWindow (application, conn, QueryWindow.Query.Q6);
				query_window.set_transient_for (this); // Set this window as the parent of the new window
				query_window.initialize ();
				query_window.show_all ();
			}
			else if (menu_item == quit) {
				application.quit ();
			}
		}

		public MainWindow (Gtk.Application application) {
			Object (application: application);

			// Load logo
			var builder = new Gtk.Builder ();
			try {
				builder.add_from_resource ("/cl/cromer/ubb/colegio/main.splash.ui");
				builder.connect_signals (null);
				content = builder.get_object ("content_grid") as Gtk.Grid;

				// Add logo to main box
				mainbox.pack_start (content, true, false, 0);
			}
			catch (Error e) {
				// This error is not fatal, so let's keep going
				warning (e.message);
			}
		}

		public void initialize () {
			try {
				conn = new Connection (HOST, PORT, OPTIONS, TTY, DATABASE, USERNAME, PASSWORD);
			}
			catch (Error e) {
				#if DEBUG
					error (e.message);
				#else
					warning (e.message);
					var msg = new Gtk.MessageDialog (this,
						Gtk.DialogFlags.MODAL,
						Gtk.MessageType.ERROR,
						Gtk.ButtonsType.CLOSE,
						"Error: No se puede conectar al base de datos!");
					msg.response.connect ((response_id) => {
						switch (response_id) {
							case Gtk.ResponseType.CLOSE:
								application.quit ();
								break;
							case Gtk.ResponseType.DELETE_EVENT:
								application.quit ();
								break;
						}
						msg.destroy ();
					});
					msg.set_title ("Error");
					msg.run ();
				#endif
			}
		}
	}
}
