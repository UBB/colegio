/*
 * Copyright 2019 Chris Cromer
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

namespace Colegio {
	namespace Constants {
		/**
		 * The Q1 statement
		 */
		public const string Q1_STATEMENT = "Mostrar el nombre de los alumnos, el nombre del apoderado, el nombre del curso cursando a la fecha, nombre del profesor jefe y profesor asistente, para aquellos alumnos que provienen de la Región del Bı́obio o Ñuble.";
		/**
		 * The Q2 statement
		 */
		public const string Q2_STATEMENT = "Mostrar el nombre de los profesores que son profesores jefes de algún curso y no tienen asignada alguna actividad extraprogramática para los niveles primero o segundo.";
		/**
		 * The Q3 statement
		 */
		public const string Q3_STATEMENT = "Mostrar el nombre de las actividades extraprogramáticas y la cantidad de alumnos inscritos, considerando alumnos que hayan nacido entre los años 2006 y 2008. Considere solo aquellas actividades extraprogramáticas que tengan más de 5 alumnos inscritos.";
		/**
		 * The Q4 statement
		 */
		public const string Q4_STATEMENT = "Mostrar los cursos del año 2019 que tengan la menor cantidad de alumnos matriculados. TIP: Genere vistas.";
		/**
		 * The Q5 statement
		 */
		public const string Q5_STATEMENT = "Muestre el nombre de las asignaturas y el promedio de notas para cada asignatura de los alumnos del curso 6A-2018 cuyos alumnos no participan en la actividad extraprogramática “Rugby”.";
		/**
		 * The Q6 statement
		 */
		public const string Q6_STATEMENT = "Muestre el nombre de las asignaturas del curso 1A-2018 que tienen un nivel de aprobación menor a 50 %. Una asignatura es aprobada si la nota es mayor o igual a 4,0. TIP: utilice vistas.";
		/**
		 * The Q1 SQL
		 */
		public const string Q1_SQL = "SELECT CONCAT_WS(' ', AL.nombres, AL.apellidos) AS alumno,
	CONCAT_WS(' ', AP.nombres, AP.apellidos) AS apoderado,
	CO.nombre curso,
	CONCAT_WS(' ', PJ.nombres, PJ.apellidos) AS jefe,
	CONCAT_WS(' ', PA.nombres, PA.apellidos) AS asistente
FROM alumno AL
JOIN apoderado AP ON (AP.rut_apoderado = AL.rut_apoderado)
JOIN cursar CU ON (CU.rut_alumno = AL.rut_alumno)
JOIN curso CO ON (CO.id_curso = CU.id_curso)
JOIN profesor PJ ON (PJ.rut_profesor = CO.rut_profesor)
LEFT JOIN asistente ASI ON (ASI.id_curso = CO.id_curso)
LEFT JOIN profesor PA ON (PA.rut_profesor = ASI.rut_profesor)
JOIN ciudad C ON (C.id_ciudad = AL.id_ciudad)
JOIN region R ON (R.id_region = C.id_region)
WHERE (
	CO.anyo = 2019 AND (
		R.nombre_region = 'Bío Bío' OR R.nombre_region = 'Ñuble'
	)
);";
		/**
		 * The Q2 SQL
		 */
		public const string Q2_SQL = "SELECT CONCAT_WS(' ', P.nombres, P.apellidos) AS profesor FROM profesor P
JOIN curso C ON (C.rut_profesor = P.rut_profesor)
WHERE P.rut_profesor NOT IN (
	SELECT P2.rut_profesor FROM profesor P2
	JOIN actividad A ON (A.rut_profesor = P2.rut_profesor)
	JOIN actividad_nivel A2 ON (A2.id_actividad = A.id_actividad)
	JOIN nivel N ON (N.id_nivel = A2.id_nivel)
	WHERE (N.nombre = 'Primero' OR N.nombre = 'Segundo')
);";
		/**
		 * The Q3 SQL
		 */
		public const string Q3_SQL = "SELECT A.nombre, COUNT(*) FROM actividad A
JOIN participar P ON (P.id_actividad = A.id_actividad)
JOIN alumno A2 ON (A2.rut_alumno = P.rut_alumno)
WHERE (A2.fecha_nacimiento >= '2006-01-01'
	AND A2.fecha_nacimiento <= '2008-12-31')
GROUP BY (A.nombre)
HAVING COUNT(*) > 5;";
		/**
		 * The Q4 SQL
		 */
		public const string Q4_SQL = "CREATE VIEW matriculados(curso, cantidad) AS (
	SELECT C1.nombre, COUNT(*) FROM curso C1
	JOIN cursar C2 ON (C2.id_curso = C1.id_curso)
	WHERE (C1.anyo = 2019)
	GROUP BY (C1.nombre)
);

SELECT M.curso, M.cantidad FROM matriculados M
WHERE (
	M.cantidad = (
		SELECT MIN(M2.cantidad) FROM matriculados M2
	)
);";
		/**
		 * The Q5 SQL
		 */
		public const string Q5_SQL = "SELECT A.nombre, AVG(r.nota) FROM asignatura A
JOIN registro R ON (R.id_asignatura = A.id_asignatura)
JOIN asociado A2 ON (A2.id_asignatura = A.id_asignatura)
JOIN alumno A3 ON (A3.rut_alumno = R.rut_alumno)
WHERE (
	A2.id_curso = '6A-2018'
	AND R.rut_alumno NOT IN (
		SELECT A4.rut_alumno FROM alumno A4
		JOIN participar P ON (P.rut_alumno = A4.rut_alumno)
		JOIN actividad A5 ON (A5.id_actividad = P.id_actividad)
		WHERE (A5.nombre = 'Rugby' AND A4.rut_alumno = A3.rut_alumno)
	)
)
GROUP BY (A.nombre);";
		/**
		 * The Q6 SQL
		 */
		public const string Q6_SQL = "CREATE VIEW total_alumnos(asignatura, cantidad) AS (
	SELECT A.id_asignatura, COUNT(*) FROM asignatura A
	JOIN registro R ON (R.id_asignatura = A.id_asignatura)
	JOIN alumno A2 ON (A2.rut_alumno = R.rut_alumno)
	JOIN asociado A3 ON (A3.id_asignatura = A.id_asignatura)
	WHERE (A3.id_curso = '1A-2018')
	GROUP BY (A.id_asignatura)
);

CREATE VIEW total_aprobados(asignatura, cantidad) AS (
	SELECT A.id_asignatura, COUNT(*) FROM asignatura A
	JOIN registro R ON (R.id_asignatura = A.id_asignatura)
	JOIN alumno A2 ON (A2.rut_alumno = R.rut_alumno)
	JOIN asociado A3 ON (A3.id_asignatura = A.id_asignatura)
	WHERE (A3.id_curso = '1A-2018' AND R.nota >= 4)
	GROUP BY (A.id_asignatura)
);

SELECT A2.nombre, TRUNC(CAST(T1.cantidad AS DECIMAL(3,2)) * 100 / CAST(T2.cantidad AS DECIMAL(3,2)),2) AS porcentaje
FROM total_aprobados T1
JOIN total_alumnos T2 ON (T2.asignatura = T1.asignatura)
JOIN asociado A ON (T1.asignatura = A.id_asignatura AND T2.asignatura = A.id_asignatura)
JOIN asignatura A2 ON (A2.id_asignatura = A.id_asignatura)
WHERE ((CAST(T1.cantidad AS DECIMAL(3,2)) * 100 / CAST(T2.cantidad AS DECIMAL(3,2))) < 50);";
	}
}
