option('debugging',
	type: 'boolean',
	value: false,
	description: 'Build with debugging preprocessor')
option('valadocs',
	type: 'boolean',
	value: true,
	description: 'Build valadocs for the application')
option('valadocs_deps',
	type: 'boolean',
	value: false,
	description: 'Build the valadocs of the deps of the application')
