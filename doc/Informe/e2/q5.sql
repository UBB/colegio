SELECT A.nombre, AVG(r.nota) FROM asignatura A
JOIN registro R ON (R.id_asignatura = A.id_asignatura)
JOIN asociado A2 ON (A2.id_asignatura = A.id_asignatura)
JOIN alumno A3 ON (A3.rut_alumno = R.rut_alumno)
WHERE (
  A2.id_curso = '6A-2018'
  AND R.rut_alumno NOT IN (
    SELECT A4.rut_alumno FROM alumno A4
    JOIN participar P ON (P.rut_alumno = A4.rut_alumno)
    JOIN actividad A5 ON (A5.id_actividad = P.id_actividad)
    WHERE (A5.nombre = 'Rugby' AND A4.rut_alumno = A3.rut_alumno)
  )
)
GROUP BY (A.nombre);
