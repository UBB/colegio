CREATE VIEW total_alumnos(asignatura, cantidad) AS (
  SELECT A.id_asignatura, COUNT(*) FROM asignatura A
  JOIN registro R ON (R.id_asignatura = A.id_asignatura)
  JOIN alumno A2 ON (A2.rut_alumno = R.rut_alumno)
  JOIN asociado A3 ON (A3.id_asignatura = A.id_asignatura)
  WHERE (A3.id_curso = '1A-2018')
  GROUP BY (A.id_asignatura)
);

CREATE VIEW total_aprobados(asignatura, cantidad) AS (
  SELECT A.id_asignatura, COUNT(*) FROM asignatura A
  JOIN registro R ON (R.id_asignatura = A.id_asignatura)
  JOIN alumno A2 ON (A2.rut_alumno = R.rut_alumno)
  JOIN asociado A3 ON (A3.id_asignatura = A.id_asignatura)
  WHERE (A3.id_curso = '1A-2018' AND R.nota >= 4)
  GROUP BY (A.id_asignatura)
);

SELECT A2.nombre, TRUNC(CAST(T1.cantidad AS DECIMAL(3,2)) * 100 / CAST(T2.cantidad AS DECIMAL(3,2)),2) AS porcentaje
FROM total_aprobados T1
JOIN total_alumnos T2 ON (T2.asignatura = T1.asignatura)
JOIN asociado A ON (T1.asignatura = A.id_asignatura AND T2.asignatura = A.id_asignatura)
JOIN asignatura A2 ON (A2.id_asignatura = A.id_asignatura)
WHERE ((CAST(T1.cantidad AS DECIMAL(3,2)) * 100 / CAST(T2.cantidad AS DECIMAL(3,2))) < 50);
