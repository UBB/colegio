SELECT A.nombre, COUNT(*) FROM actividad A
JOIN participar P ON (P.id_actividad = A.id_actividad)
JOIN alumno A2 ON (A2.rut_alumno = P.rut_alumno)
WHERE (A2.fecha_nacimiento >= '2006-01-01'
  AND A2.fecha_nacimiento <= '2008-12-31')
GROUP BY (A.nombre)
HAVING COUNT(*) > 5;
