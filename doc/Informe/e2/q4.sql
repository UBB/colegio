CREATE VIEW matriculados(curso, cantidad) AS (
  SELECT C1.nombre, COUNT(*) FROM curso C1
  JOIN cursar C2 ON (C2.id_curso = C1.id_curso)
  WHERE (C1.anyo = 2019)
  GROUP BY (C1.nombre)
);

SELECT M.curso, M.cantidad FROM matriculados M
WHERE (
  M.cantidad = (
    SELECT MIN(M2.cantidad) FROM matriculados M2
  )
);
