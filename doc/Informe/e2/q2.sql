SELECT CONCAT_WS(' ', P.nombres, P.apellidos) AS profesor FROM profesor P
JOIN curso C ON (C.rut_profesor = P.rut_profesor)
WHERE P.rut_profesor NOT IN (
  SELECT P2.rut_profesor FROM profesor P2
  JOIN actividad A ON (A.rut_profesor = P2.rut_profesor)
  JOIN actividad_nivel A2 ON (A2.id_actividad = A.id_actividad)
  JOIN nivel N ON (N.id_nivel = A2.id_nivel)
  WHERE (N.nombre = 'Primero' OR N.nombre = 'Segundo')
);
