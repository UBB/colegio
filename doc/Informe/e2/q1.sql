SELECT CONCAT_WS(' ', AL.nombres, AL.apellidos) AS alumno,
  CONCAT_WS(' ', AP.nombres, AP.apellidos) AS apoderado,
  CO.nombre curso,
  CONCAT_WS(' ', PJ.nombres, PJ.apellidos) AS jefe,
  CONCAT_WS(' ', PA.nombres, PA.apellidos) AS asistente
FROM alumno AL
JOIN apoderado AP ON (AP.rut_apoderado = AL.rut_apoderado)
JOIN cursar CU ON (CU.rut_alumno = AL.rut_alumno)
JOIN curso CO ON (CO.id_curso = CU.id_curso)
JOIN profesor PJ ON (PJ.rut_profesor = CO.rut_profesor)
JOIN asistente ASI ON (ASI.id_curso = CO.id_curso)
JOIN profesor PA ON (PA.rut_profesor = ASI.rut_profesor)
JOIN ciudad C ON (C.id_ciudad = AL.id_ciudad)
JOIN region R ON (R.id_region = C.id_region)
WHERE (
  CO.anyo = 2019 AND (
    R.nombre_region = 'Bío Bío' OR R.nombre_region = 'Ñuble'
  )
);
