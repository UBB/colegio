INSERT INTO region (id_region, nombre_region) VALUES (1, 'Florida');
INSERT INTO region (id_region, nombre_region) VALUES (2, 'Alabama');
INSERT INTO region (id_region, nombre_region) VALUES (3, 'Virginia');

INSERT INTO ciudad (id_ciudad, nombre_ciudad, id_region) VALUES (1, 'Pensacola', 1);
INSERT INTO ciudad (id_ciudad, nombre_ciudad, id_region) VALUES (2, 'Montgomery', 2);
INSERT INTO ciudad (id_ciudad, nombre_ciudad, id_region) VALUES (3, 'Virginia Beach', 3);

INSERT INTO apoderado (rut_apoderado, nombres, apellidos, direccion, id_ciudad) VALUES ('14592927k', 'Christopher Barry', 'Cromer', '214 Edison Dr.', 1);
INSERT INTO apoderado (rut_apoderado, nombres, apellidos, direccion, id_ciudad) VALUES ('192186099', 'Elia Alejandra', 'Chacano Troncoso', 'Avenida San Sebastian 778', 2);
INSERT INTO apoderado (rut_apoderado, nombres, apellidos, direccion, id_ciudad) VALUES ('192199859', 'Jack', 'Johnson', '320 Thomas Jefferson St.', 3);

INSERT INTO alumno (rut_alumno, nombres, apellidos, direccion, anyo_ingreso, fecha_nacimiento, rut_apoderado, id_ciudad) VALUES ('254049492', 'Izabella Lynn', 'Cromer Chacano', '214 Edison Dr.', 2019, '2019-10-04', '14592927k', 1);
INSERT INTO alumno (rut_alumno, nombres, apellidos, direccion, anyo_ingreso, fecha_nacimiento, rut_apoderado, id_ciudad) VALUES ('245191391', 'Tomas', 'Bunt', 'Avenida San Sebastian 778', 2018, '2010-02-05', '192186099', 2);
INSERT INTO alumno (rut_alumno, nombres, apellidos, direccion, anyo_ingreso, fecha_nacimiento, rut_apoderado, id_ciudad) VALUES ('18028642k', 'Tom', 'Riddle', '320 Thomas Jefferson St.', 2017, '2014-12-31', '192199859', 3);

INSERT INTO profesor (rut_profesor, nombres, apellidos, direccion, id_ciudad) VALUES ('226174788', 'Severus', 'Snape', '232 Jackson Ave.', 1);
INSERT INTO profesor (rut_profesor, nombres, apellidos, direccion, id_ciudad) VALUES ('226779329', 'Minerva', 'McGonagall', '333 Nottingham Ln.', 2);
INSERT INTO profesor (rut_profesor, nombres, apellidos, direccion, id_ciudad) VALUES ('139414853', 'Albus', 'Dumbledore', '8450 Sparrow Rd.', 3);

INSERT INTO actividad (id_actividad, nombre, cupos, lugar, rut_profesor) VALUES (1, 'Quidditch', 14, 'Quidditch field', '226174788');
INSERT INTO actividad (id_actividad, nombre, cupos, lugar, rut_profesor) VALUES (2, 'Ogre taming', 5, 'The forbidden forest', '226779329');
INSERT INTO actividad (id_actividad, nombre, cupos, lugar, rut_profesor) VALUES (3, 'Advanced transfiguration', 2, 'Headmaster''s office', '139414853');

INSERT INTO nivel (id_nivel, nombre) VALUES (1, 'First');
INSERT INTO nivel (id_nivel, nombre) VALUES (2, 'Second');
INSERT INTO nivel (id_nivel, nombre) VALUES (3, 'Third');

INSERT INTO bloque (id_bloque, descripcion) VALUES (1, '08:10 - 09:30');
INSERT INTO bloque (id_bloque, descripcion) VALUES (2, '09:40 - 11:00');
INSERT INTO bloque (id_bloque, descripcion) VALUES (3, '11:10 - 12:30');

INSERT INTO curso (id_curso, rut_profesor) VALUES ('1A-2017', '226174788');
INSERT INTO curso (id_curso, rut_profesor) VALUES ('2B-2018', '226779329');
INSERT INTO curso (id_curso, rut_profesor) VALUES ('3C-2019', '139414853');

INSERT INTO asignatura (id_asignatura, nombre, rut_profesor) VALUES (1, 'Transfiguration', '226779329');
INSERT INTO asignatura (id_asignatura, nombre, rut_profesor) VALUES (2, 'Defense against the dark arts', '226174788');
INSERT INTO asignatura (id_asignatura, nombre, rut_profesor) VALUES (3, 'Muggle studies', '139414853');

INSERT INTO participar (rut_alumno, id_actividad) VALUES ('254049492', 1);
INSERT INTO participar (rut_alumno, id_actividad) VALUES ('245191391', 2);
INSERT INTO participar (rut_alumno, id_actividad) VALUES ('18028642k', 3);

INSERT INTO actividad_nivel (id_actividad, id_nivel) VALUES (1, 1);
INSERT INTO actividad_nivel (id_actividad, id_nivel) VALUES (2, 2);
INSERT INTO actividad_nivel (id_actividad, id_nivel) VALUES (3, 3);

INSERT INTO actividad_bloque (id_actividad, id_bloque) VALUES (1, 1);
INSERT INTO actividad_bloque (id_actividad, id_bloque) VALUES (2, 2);
INSERT INTO actividad_bloque (id_actividad, id_bloque) VALUES (3, 3);

INSERT INTO horario (id_asignatura, id_bloque, sala) VALUES (1, 1, '203AC');
INSERT INTO horario (id_asignatura, id_bloque, sala) VALUES (2, 2, '301AB');
INSERT INTO horario (id_asignatura, id_bloque, sala) VALUES (3, 3, '102AA');

INSERT INTO registro (rut_alumno, id_asignatura, nota) VALUES ('254049492', 1, 7.0);
INSERT INTO registro (rut_alumno, id_asignatura, nota) VALUES ('245191391', 2, 4.0);
INSERT INTO registro (rut_alumno, id_asignatura, nota) VALUES ('18028642k', 3, 1.0);

INSERT INTO cursar (rut_alumno, id_curso) VALUES ('254049492', '1A-2017');
INSERT INTO cursar (rut_alumno, id_curso) VALUES ('245191391', '2B-2018');
INSERT INTO cursar (rut_alumno, id_curso) VALUES ('18028642k', '3C-2019');

INSERT INTO asociado (id_curso, id_asignatura) VALUES ('1A-2017', 1);
INSERT INTO asociado (id_curso, id_asignatura) VALUES ('2B-2018', 2);
INSERT INTO asociado (id_curso, id_asignatura) VALUES ('3C-2019', 3);

INSERT INTO asistente (rut_profesor, id_curso) VALUES ('139414853', '1A-2017');
INSERT INTO asistente (rut_profesor, id_curso) VALUES ('226174788', '2B-2018');
INSERT INTO asistente (rut_profesor, id_curso) VALUES ('226779329', '3C-2019');
